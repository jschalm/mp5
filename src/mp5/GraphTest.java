package mp5;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

/**
 * @author Jordan Schalm
 * 
 * For testing the graph class.
 */
public class GraphTest {
	
	Graph graph = new Graph();
	
	@Before
	public void addVerticesTest() {
		graph.addVertex("Jason", "a");
		graph.addVertex("Jason", "p");
		graph.addVertex("Jason", "l");
		graph.addVertex("Jason", "b");
		graph.addVertex("Jason", "r");
		
		graph.addVertex("Mary", "a");
		graph.addVertex("Mary", "p");
		graph.addVertex("Mary", "s");
		graph.addVertex("Mary", "d");
		
		graph.addVertex("Leah", "d");
		graph.addVertex("Leah", "l");
		
		graph.addVertex("Al", "b");
		graph.addVertex("Al", "r");
		graph.addVertex("Al", "t");
		
		graph.addVertex("Robert", "s");
		graph.addVertex("Robert", "c");
		graph.addVertex("Robert", "m");
		
		graph.addVertex("Emma", "c");
		graph.addVertex("Emma", "n");
		graph.addVertex("Emma", "o");
		graph.addVertex("Emma", "q");
		graph.addVertex("Emma", "z");
		
		graph.addVertex("Daryl", "t");
		graph.addVertex("Daryl", "g");
		graph.addVertex("Daryl", "z");
		
		graph.addVertex("Cory", "g");
		
		graph.addVertex("Sage", "m");
		graph.addVertex("Sage", "n");
		
		graph.addVertex("Troy", "o");
		
		graph.addVertex("Ned", "q");
		
		graph.addVertex("Zoe", "no_neighbours");
		
	}
	
	/**
	 * Tests that the search will return the lexicographically lowest character at each path.
	 */
	@Test
	public void pathTest() throws NoSuchVertexException, NoPathException {
		graph.reconcileEdges();
		System.out.println("Finding shortest path from Jason to Emma.\nShould be [b,t,z]");
		System.out.println(graph.getShortestPath("Jason", "Emma"));
		System.out.println(graph.getShortestPathMultiThreaded("Jason", "Emma", 5));
		System.out.println();
		
		System.out.println("Finding shortest path from Al to Robert.\nShould be [t,z,c]");
		System.out.println(graph.getShortestPath("Al", "Robert"));
		System.out.println(graph.getShortestPathMultiThreaded("Al", "Robert", 8));
		System.out.println();
		
		System.out.println("Finding shortest path from Troy to Jason.\nShould be [o,z,t,b]");
		System.out.println(graph.getShortestPath("Troy", "Jason"));
		System.out.println(graph.getShortestPathMultiThreaded("Troy", "Jason", 3));
		System.out.println();
	}
	
	@Test
	public void noPathTest() {
		graph.reconcileEdges();
		try {
			graph.getShortestPath("Zoe", "Emma");
			fail();
		} catch( NoPathException npe ) {
			System.out.println("No path");
			assertTrue(true);
		} catch (NoSuchVertexException e) {
			fail();
		}
		try {
			System.out.println(graph.getShortestPathMultiThreaded("Zoe", "Cory", 3));
			fail();
		} catch( NoPathException npe ) {
			System.out.println("No path");
			assertTrue(true);
		} catch (NoSuchVertexException e) {
			fail();
		}
	}
}
