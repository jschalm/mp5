package mp5;

/**
 * @author jordan
 *
 * NoSuchVertexException is thrown when no vertex is found for a given name.
 */
public class NoSuchVertexException extends Exception {
	private static final long serialVersionUID = 5941354957539389956L;
}
