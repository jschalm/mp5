package mp5;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;
import java.util.TreeSet;


/**
 * @author jordan
 *
 * Represents a vertex in a graph.
 * 
 * RI: This vertex cannot share an edge with itself. All edges that connect to this node must
 *     be in this.edges and vice versa. There can be no duplicate entries in this.edges.values().
 *      this.name, this.edges, this.apperances != null.
 *      
 * AF: An instance of Vertex represents a vertex in a graph with edges.size() edges connecting
 *     between this vertex and each other vertex specified by edges.keySet() with edge labels 
 *     between this vertex and any vertex v given by edges.get(v).
 */
public class Vertex implements Comparable<Vertex> {
	private String name;
	private Map<Vertex, TreeSet<String>> edges;
	private Set<String> appearances;
	
	/**
	 * Creates a vertex with a given name.
	 * @param name
	 * 				The name of the vertex to create.
	 */
	public Vertex( String name, String appearance ) {
		this.name = name;
		edges = new TreeMap<Vertex, TreeSet<String>>();
		appearances = new TreeSet<String>();
		appearances.add(appearance);
	}
	
	/**
	 * Adds an edge to this vertex.
	 * @param otherVertex
	 * 				Vertex to add an edge to.
	 * @param commonAppearance
	 * 				Label for the new edge, or appearance to be
	 * 				added to the label of the existing edge.
	 */
	public void addEdge( Vertex otherVertex, TreeSet<String> commonAppearances ) {
		edges.put(otherVertex, commonAppearances);
	}
	
	/**
	 * Adds an appearance title to this.appearances
	 * @param appearance
	 * 					The title of the appearance to add.
	 */
	public void addAppearance( String appearance ) {
		appearances.add(appearance);
	}
	
	/**
	 * @return the name of the vertex 
	 */
	public String getName() {
		return new String(name);
	}
	
	/**
	 * @return a clone of this vertex's appearances set
	 */
	public synchronized TreeSet<String> getAppearances() {
		TreeSet<String> clone = new TreeSet<String>();
		for( String s : appearances ) {
			clone.add(s);
		}
		return clone;
	}
	
	/**
	 * Checks whether or not this vertex shares an appearance with
	 * another vertex.
	 * @param other
	 * 			The other vertex to compare to.
	 * @return true if the two vertices share at least one appearance, 
	 * 		   false otherwise.
	 */
	public boolean hasCommonAppearanceWith(Vertex other) {
		Set<String> intersection = getAppearances();
		intersection.retainAll(other.getAppearances());
		return intersection.size() > 0;
	}
	
	/**
	 * @return a copy of this vertex's list of neighbours
	 */
	public synchronized Set<Vertex> getNeighbours() {
		Set<Vertex> neighbours = new TreeSet<Vertex>();
		for( Vertex v : edges.keySet() ) {
			neighbours.add(v);
		}
		return neighbours;
	}
	
	/**
	 * Finds the lexicographically lowest common appearance with one of
	 * this vertex's neighbours
	 * @param neighbour
	 * 				Vertex for which to find appearance.
	 * @requires neighbour must share an edge with this vertex.
	 * @return The title of the lexicographically lowest common appearance.
	 */
	public String getCommonAppearanceWith( Vertex neighbour ) {
		return edges.get(neighbour).first();
	}
		
	/**
	 * Two vertices are identical if they have the same name.
	 * @return true if the hashcode matches and the name matches, false otherwise
	 */		
	@Override 
	public boolean equals(Object object) {
		if(!(object instanceof Vertex)){
			return false; 
		}
		Vertex newVertex = (Vertex) object; 
		if( !(this.hashCode() == newVertex.hashCode()) ) {
			return false;
		}
		if( this.getName().equals(newVertex.getName()) ){
			return true;
		}
		return false;		
	}
	
	@Override
	public int hashCode() {
		try {
		return (int) name.charAt(0) * (int) name.charAt(name.length() - 1); 
		} catch( StringIndexOutOfBoundsException sioobe ) {
			return -1;
		}
	}
	
	@Override
	public int compareTo(Vertex other) {
		if(this.name.equals(other.name)){
			return 0;
		}
		List<String> names = new ArrayList<String>();
		names.add(this.name);
		names.add(other.name);
		Collections.sort(names);
		if(names.get(0).equals(this.name)){
			return -1;
		}
		return 1;
	}
	
	@Override
	public String toString() {
		return name;
	}
}
