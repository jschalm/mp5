package mp5;

import static org.junit.Assert.*;

import java.io.IOException;

import org.junit.Before;
import org.junit.Test;

public class VertexTest {

	
	Vertex vertex1 = new Vertex("Vertex", "A Brave Refrain");
	Vertex vertex2 = new Vertex("Vert", "Harry Potter");
	Vertex vertex3 = new Vertex("Vert", "Harry Potter");
	Vertex vertex4 = new Vertex("Vtx", "Lord of the Rings");

	
	@Before
	public void setup() throws IOException {
		
		Vertex vertex1 = new Vertex("Vertex1", "A Brave Refrain");
		Vertex vertex2 = new Vertex("Vertex2", "Harry Potter");
		Vertex vertex3 = new Vertex("Vertex2", "Harry Potter");
		Vertex vertex4 = new Vertex("Vertex3", "Lord of the Rings");
		
		vertex1.addAppearance("Game of Thrones");
		vertex1.addAppearance("Fifty Shades of Grey");
		vertex2.addAppearance("The Fault in our Stars");
		vertex2.addAppearance("Looking for Alaska");
		vertex3.addAppearance("Looking for Alaska");
		vertex3.addAppearance("The Fault in our Stars");
		vertex4.addAppearance("The Body Book");
		vertex4.addAppearance("Life of Pi");
		
	}
	
	@Test
	public void TestHashCode(){
		assertTrue(vertex2.hashCode()==vertex3.hashCode());
		assertTrue(vertex1.hashCode()!=vertex4.hashCode());
		
	}
	
	@Test
	public void TestEquals(){
		assertTrue(vertex2.equals(vertex3));
		assertTrue(!(vertex2.equals(vertex1)));
		
	}
	
	@Test
	public void TestName(){
		assertEquals("Vertex",vertex1.getName());
		assertEquals("Vert",vertex2.getName());
		assertFalse(vertex3.getName().equals("Vertex"));
		
	}
	
	
}
