package mp5;

/**
 * @author jordan
 *
 * NoPath Exception is thrown when there is no path between two vertices.s
 */
public class NoPathException extends Exception {
	private static final long serialVersionUID = 8441336835015649829L;
}
