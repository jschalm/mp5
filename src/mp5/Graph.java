package mp5;

import java.util.Queue;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.TimeUnit;
import java.util.LinkedList;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;
import java.util.TreeSet;
import java.util.ArrayList;


/**
 * @author Jordan Schalm
 * 
 * Represents a graph with n vertices and m edges.
 * 
 * RI: Vertices must have a unique name. Vertices and edges can be added but not removed. Vertices cannot share 
 *     an edge with themselves (no self-referentiality) and cannot share more than one edge with any other vertex
 *     (not a multigraph). this.vertices != null.
 *
 * AF: An instance of Graph with vertices.size() == n corresponds to a graph with n vertices. For each vertex v, 
 * 	   all neighbours with which it shares an edge are given by v.edges.keySet() and the label for each edge
 * 	   between v and all neighbours w is given by v.edges.get(w).
 */
public class Graph {
	private Set<Vertex> vertices;
	
	public Graph() {
		this.vertices = new TreeSet<Vertex>();
	}
	
	/**
	 * Adds a vertex to this graph if a vertex with the given name
	 * does not exist in the graph, otherwise adds the appearance
	 * given to the vertex with the given name.
	 * @param name
	 * 			The name of the vertex.
	 * @return true if a new vertex was added, false if a vertex
	 * 		   with the same name already exists in this graph.
	 */
	public boolean addVertex( String name, String appearance ) {
		try {
			Vertex existingVertex = this.getVertex(name);
			existingVertex.addAppearance(appearance);
			return false;
		} catch( NoSuchVertexException nsve ) {
			Vertex newVertex = new Vertex(name, appearance);
			return vertices.add(newVertex);
		}
	}
	
	/**
	 * Returns a string array showing the edge titles in the shortest
	 * path between two vertices. If there is more than one path, returns 
	 * the lexicographically shortest path. If there is no path, throws 
	 * a NoPathException.
	 * @param vertex1
	 * 				The name of the source vertex.
	 * @param vertex2
	 * 				The name of the destination vertex.
	 * @return ArrayList of Strings containing the edges along the
	 * 		   lexicographically shortest path from source to vertex.
	 * @throws NoPathException if there exists no path between the given
	 * 		   vertices.
	 * @throws NoSuchVertexException if one of the given vertices does
	 * 		   not exist in the graph.
	 */
	// Single threaded implementation //
	public ArrayList<String> getShortestPath( String vertex1, String vertex2 ) throws NoSuchVertexException, NoPathException {
		Vertex start = getVertex(vertex1);
		Vertex destination = getVertex(vertex2);
		Queue<Vertex> workList = new LinkedList<Vertex>();
		Map<Vertex,ArrayList<String>> pathMap = new TreeMap<Vertex,ArrayList<String>>();
		ArrayList<String> currentPath = new ArrayList<String>();
		ArrayList<String> newPath;
		
		workList.add(start);
		pathMap.put(start, new ArrayList<String>() );
		
		Vertex current;
		while( !workList.isEmpty() ) {
			current = workList.poll();
			if( current == destination ) {
				return pathMap.get(destination);
			}
			for( Vertex neighbour : current.getNeighbours() ) {
				if( !pathMap.keySet().contains(neighbour) ) {
					currentPath = pathMap.get(current);
					newPath = new ArrayList<String>(currentPath);
					newPath.add(neighbour.toString() + " in " + current.getCommonAppearanceWith(neighbour));
					pathMap.put(neighbour, newPath);
					workList.add(neighbour);
				}
			}
		}
		throw new NoPathException();
	}
	
	/**
	 * Returns a string array showing the edge titles in the shortest
	 * path between two vertices. If there is more than one path, returns 
	 * the lexicographically shortest path. If there is no path, throws 
	 * a NoPathException. 
	 * @param vertex1
	 * 				The name of the source vertex.
	 * @param vertex2
	 * 				The name of the destination vertex.
	 * @param numThreads
	 * 				The number of threads with which to perform the search,
	 * 				not including the Main thread.
	 * @return ArrayList of Strings containing the edges along the
	 * 		   lexicographically shortest path from source to vertex.
	 * @throws NoSuchVertexException if one or both of the vertices does not 
	 * 		   exist in the graph.
	 * @throws NoPathException if no path exists between the two input vertices.
	 * 
	 * Thread Safety Argument: getShortestPathMultiThreaded has a LinkedBlockingQueue
	 * 	    and a ConcurrentHashMap as shared resources, both of which are thread-safe
	 * 		data types, which will conserve the integrity of the shared path data
	 *      and work-list data. All accesses to instances of Vertex are synchronized
	 *      and occur in alphabetical order which will prevent deadlock. Although
	 *      vertices are removed from the queue in order, it is possible that they may
	 *      be processed out of order which ruins the automatic lexicographical ordering
	 *      of paths guaranteed in the single threaded method. As a result, an additional 
	 *      method is added to check that a path is the lexicographically lowest before 
	 *      it is added to the map. Additionally, if the queue finishes processing out of
	 *      order and the first path found is not the lexicographically lowest, all other 
	 *      possible paths of the same length will nevertheless be checked because they 
	 *      occur before the poison pills in the queue. This guarantees that processing
	 *      out of order will still return the correct path. The threads are guaranteed to 
	 *      finish running gracefully because if a path is found, poison pills are added to 
	 *      the end of the queue to make all threads break from their loop. If no path is 
	 *      found, each thread will throw an InterruptedException after 1 second and break 
	 *      from their loop and finish gracefully.
	 */
	public ArrayList<String> getShortestPathMultiThreaded( String vertex1, String vertex2, int numThreads ) 
			throws NoSuchVertexException, NoPathException {
		Vertex start = getVertex(vertex1);
		Vertex destination = getVertex(vertex2);
		BlockingQueue<Vertex> workList = new LinkedBlockingQueue<Vertex>();
		ConcurrentMap<Vertex,ArrayList<String>> pathMap = new ConcurrentHashMap<Vertex,ArrayList<String>>();
		
		workList.add(start);
		pathMap.put(start,new ArrayList<String>());
		
		Thread[] pathFinders = new Thread[numThreads];
		for( int i = 0; i < numThreads; i++) { 
			Thread pathFinder = pathFinders[i] = new Thread( new PathFinder( workList, pathMap, destination, numThreads ) );
			pathFinder.start();
		}
		
		// Wait for all threads to terminate...
		for( Thread pathFinder : pathFinders ) {
			try {
				pathFinder.join(0);
			} catch (InterruptedException e) {
				System.out.println("InterruptedException thrown in Main thread.");
			}
		}
		if(pathMap.get(destination) == null) {
			throw new NoPathException();
		}
		return pathMap.get(destination);
	}
	
	/**
	 * Returns a vertex in this graph, given that vertex's name.
	 * 
	 * @param name
	 * 			Name of the vertex to find.
	 * @return Vertex in this graph with the given name, if it is found.
	 * @throws NoSuchVertexException if no vertex exists in this graph with
	 * 		   the given name
	 */
	private Vertex getVertex( String name ) throws NoSuchVertexException {
		 for( Vertex vertex : this.vertices ) {
			 if( vertex.getName().equals(name) ) {
				 return vertex;
			 }
		 }
		 throw new NoSuchVertexException();
	 }
	
	/**
	 * After all vertices have been created, this method reconciles their 
	 * common appearances by creating all relevant edges.
	 */
	public void reconcileEdges() {
		ArrayList<Vertex> unvisited = new ArrayList<Vertex>(vertices);
		TreeSet<String> commonAppearances;
		for( Vertex current : vertices ) {
			unvisited.remove(current);
			for( Vertex other : unvisited ) {
				if( current.hasCommonAppearanceWith(other) ) {
					commonAppearances = current.getAppearances();
					commonAppearances.retainAll(other.getAppearances());
					current.addEdge(other, commonAppearances);
					other.addEdge(current, commonAppearances);
				}
			}
		}
	}
	
	@Override
	public int hashCode() {
		return vertices.size();
	}
}

class PathFinder implements Runnable {
	private BlockingQueue<Vertex> workList;
	private ConcurrentMap<Vertex, ArrayList<String>> pathMap;
	private final Vertex destination;
	private final int numThreads;
	
	public PathFinder( BlockingQueue<Vertex> workList, ConcurrentMap<Vertex, ArrayList<String>> pathMap, Vertex destination, int numThreads ) {
		this.workList = workList;
		this.pathMap = pathMap;
		this.destination = destination;
		this.numThreads = numThreads;
	}
	
	@Override
	public void run() {
		Vertex current;
		ArrayList<String> currentPath;
		ArrayList<String> newPath;	
		while( true ) {
			try {
				current = workList.poll(1, TimeUnit.SECONDS);
				if( current == destination ) {
					// The search is over and we have found a path.
					// Add poison pills to the end of the queue to terminate all the other threads.
					for( int i = 0; i < numThreads; i++ ) {
						workList.put(null);
					}
					break;
				}
				for( Vertex neighbour : current.getNeighbours() ) {
					currentPath = pathMap.get(current);
					newPath = new ArrayList<String>(currentPath);
					newPath.add(neighbour.toString() + " in " + current.getCommonAppearanceWith(neighbour));
					if( !pathMap.containsKey(neighbour) ) {
						pathMap.put(neighbour, newPath);
						workList.put(neighbour);
					}
					// If there is already a path associated with neighbour, but the new path would be 
					// lexicographically less, then replace the old path with the new path.
					else if( newPathIsLexicographicallyLessThanOldPath(newPath, pathMap.get(neighbour)) ) {
						pathMap.put(neighbour, newPath);
						// Don't add neighbour to workList because it's already there.
					}
				}
			} catch (InterruptedException ie) {
				break;
			} catch (NullPointerException npe) {
				break;
			}
		}
	}
	
	/**
	 * Compares a new and old path. true if the new path is both lexicographically
	 * less than and equal to in length or shorter than the old path, otherwise returns false.
	 * @param newPath
	 * 				The new path to compare.
	 * @param oldPath
	 * 				The old path to compare
	 * @return true if the new path is both lexicographically less than and equal to in length 
	 *         or shorter than the old path, otherwise returns false.
	 */
	private boolean newPathIsLexicographicallyLessThanOldPath( ArrayList<String> newPath, ArrayList<String> oldPath) {
		if( newPath.size() > oldPath.size() ) {
			return false;
		}
		if( newPath.size() < oldPath.size() ) {
			return true;
		}
		for( int i = 0; i < newPath.size(); i++  ) {
			// If new[i].compareTo(old[i]) < 0 then new[i] is lexicographically less than old[i]
			if( newPath.get(i).compareTo(oldPath.get(i)) > 0 ) {
				return false;
			}
		}
		return true;
	}
}